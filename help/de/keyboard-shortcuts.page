<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="keyboard-shortcuts" xml:lang="de">

  <info>
    <link type="guide" xref="index#options"/>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Das Senden von Tastenkürzeln zum entfernten Rechner verhindern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2009, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2009,2014,2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Tastenkombinationen</title>

  <p>Sie können Tastenkürzel durch Klicken auf <guiseq><gui style="menu">Ansicht</gui><gui style="menuitem">Tastenkürzel</gui></guiseq> ein- oder ausschalten.</p>

  <p>Wenn Sie diese Option wählen, werden Tastenkürzel (wie <keyseq><key>Strg</key><key>N</key></keyseq>) und Menü-Zugriffstasten (wie <keyseq><key>Alt</key><key>M</key></keyseq>) sowie Mnemonics in <app>Betrachter für entfernte Bildschirme</app> aktiviert, die normalerweise dem Zugriff auf das Menü des entfernten Rechners dienen. Das bedeutet, dass <app>Betrachter für entfernte Bildschirme</app> diese Tastenkombinationen abfängt, anstelle sie an den entfernten Rechner weiterzuleiten. Meist werden Sie jedoch auf dem entfernten Rechner arbeiten wollen, daher ist diese Option per Vorgabe deaktiviert.</p>

  <note>
    <p>Wenn die Einstellung <gui>Tastenkürzel</gui> deaktiviert ist, dann ist <keyseq><key>Strg</key><key>Alt</key><key>Entf</key></keyseq> die einzige Tastenkombination, die nicht an den entfernten Rechner gesendet wird. Klicken Sie in diesem Fall auf den <guiseq><gui style="menu">Entfernt</gui><gui style="menuitem">Strg-Alt-Entf senden</gui></guiseq>-Knopf in der Werkzeugleiste, um diese Tastenkombination zu senden.</p>
  </note>

</page>
